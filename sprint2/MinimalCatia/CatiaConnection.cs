﻿using System;
using System.Windows;
using HybridShapeTypeLib;
using INFITF;
using MECMOD;
using PARTITF;


namespace MinimalCatia
{
    class CatiaConnection
    {
        INFITF.Application hsp_catiaApp;
        MECMOD.PartDocument hsp_catiaPartDoc;
        MECMOD.Sketch hsp_catiaSkizze;

        ShapeFactory SF;
        HybridShapeFactory HSF;

        Part myPart;
        Sketches mySketches;

        public bool CATIALaeuft()
        {
            try
            {
                object catiaObject = System.Runtime.InteropServices.Marshal.GetActiveObject(
                    "CATIA.Application");
                hsp_catiaApp = (INFITF.Application) catiaObject;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Boolean ErzeugePart()
        {
            INFITF.Documents catDocuments1 = hsp_catiaApp.Documents;
            hsp_catiaPartDoc = catDocuments1.Add("Part") as MECMOD.PartDocument;
            myPart = hsp_catiaPartDoc.Part;

            return true;
        }

        public void ErstelleLeereSkizze()
        {
            // Factories für das Erzeugen von Modellelementen (Std und Hybrid)
            SF = (ShapeFactory)myPart.ShapeFactory;
            HSF = (HybridShapeFactory)myPart.HybridShapeFactory;

            // geometrisches Set auswaehlen und umbenennen
            HybridBodies catHybridBodies1 = myPart.HybridBodies;
            HybridBody catHybridBody1;
            try
            {
                catHybridBody1 = catHybridBodies1.Item("Geometrisches Set.1");
            }
            catch (Exception)
            {
                MessageBox.Show("Kein geometrisches Set gefunden! " + Environment.NewLine +
                    "Ein PART manuell erzeugen und ein darauf achten, dass 'Geometisches Set' aktiviert ist.",
                    "Fehler", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            catHybridBody1.set_Name("Profile");

            // neue Skizze im ausgewaehlten geometrischen Set auf eine Offset Ebene legen
            mySketches = catHybridBody1.HybridSketches;
            OriginElements catOriginElements = myPart.OriginElements;
            HybridShapePlaneOffset hybridShapePlaneOffset1 = HSF.AddNewPlaneOffset(
                (Reference)catOriginElements.PlaneYZ, 90.000000, false);
            hybridShapePlaneOffset1.set_Name("OffsetEbene");
            catHybridBody1.AppendHybridShape(hybridShapePlaneOffset1);
            myPart.InWorkObject = hybridShapePlaneOffset1;
            myPart.Update();

            HybridShapes hybridShapes1 = catHybridBody1.HybridShapes;
            Reference catReference1 = (Reference)hybridShapes1.Item("OffsetEbene");

            hsp_catiaSkizze = mySketches.Add(catReference1);

            // Achsensystem in Skizze erstellen 
            ErzeugeAchsensystem();

            // Part aktualisieren
            myPart.Update();
        }

        private void ErzeugeAchsensystem()
        {
            object[] arr = new object[] {0.0, 0.0, 0.0,
                                         0.0, 1.0, 0.0,
                                         0.0, 0.0, 1.0 };
            hsp_catiaSkizze.SetAbsoluteAxisData(arr);
        }

        public void ErzeugeProfil(Double b, Double h)
        {
            // Skizze umbenennen
            hsp_catiaSkizze.set_Name("Rechteck");

            // Rechteck in Skizze einzeichnen
            // Skizze oeffnen
            Factory2D catFactory2D1 = hsp_catiaSkizze.OpenEdition();
            /*
            // Rechteck erzeugen
            
            // erst die Punkte
            Point2D catPoint2D1 = catFactory2D1.CreatePoint(-50, 50);
            Point2D catPoint2D2 = catFactory2D1.CreatePoint(50, 50);
            Point2D catPoint2D3 = catFactory2D1.CreatePoint(50, -50);
            Point2D catPoint2D4 = catFactory2D1.CreatePoint(-50, -50);

            // dann die Linien
            Line2D catLine2D1 = catFactory2D1.CreateLine(-50, 50, 50, 50);
            catLine2D1.StartPoint = catPoint2D1;
            catLine2D1.EndPoint = catPoint2D2;

            Line2D catLine2D2 = catFactory2D1.CreateLine(50, 50, 50, -50);
            catLine2D2.StartPoint = catPoint2D2;
            catLine2D2.EndPoint = catPoint2D3;

            Line2D catLine2D3 = catFactory2D1.CreateLine(50, -50, -50, -50);
            catLine2D3.StartPoint = catPoint2D3;
            catLine2D3.EndPoint = catPoint2D4;

            Line2D catLine2D4 = catFactory2D1.CreateLine(-50, -50, -50, 50);
            catLine2D4.StartPoint = catPoint2D4;
            catLine2D4.EndPoint = catPoint2D1;
            */
            // Polygon erzeugen

            // erst die Punkte
            Point2D catPoint2D1 = catFactory2D1.CreatePoint(5.773503, 10.000000);
            Point2D catPoint2D2 = catFactory2D1.CreatePoint(-5.773503, 10.000000);
            Point2D catPoint2D3 = catFactory2D1.CreatePoint(-11.547005, 0.000000);
            Point2D catPoint2D4 = catFactory2D1.CreatePoint(-5.773503, -10.000000);
            Point2D catPoint2D5 = catFactory2D1.CreatePoint(5.773503, -10.000000);
            Point2D catPoint2D6 = catFactory2D1.CreatePoint(11.547005, -0.000000);

            Point2D catPoint2D7 = catFactory2D1.CreatePoint(0.000000, 10.000000);
            Circle2D circle2D2 = catFactory2D1.CreateClosedCircle(0.000000, 0.000000, 11.547005);
            Circle2D circle2D3 = catFactory2D1.CreateClosedCircle(0.000000, 0.000000, 10.000000);
            circle2D2.Construction = true;
            circle2D3.Construction = true;


            // dann die Linien
            Line2D catLine2D1 = catFactory2D1.CreateLine(5.773503, 10.000000, -5.773503, 10.000000);
            catLine2D1.StartPoint = catPoint2D1;
            catLine2D1.EndPoint = catPoint2D2;

            Line2D catLine2D2 = catFactory2D1.CreateLine(-5.773503, 10.000000, -11.547005, 0.000000);
            catLine2D2.StartPoint = catPoint2D2;
            catLine2D2.EndPoint = catPoint2D3;

            Line2D catLine2D3 = catFactory2D1.CreateLine(-11.547005, 0.000000, -5.773503, -10.000000);
            catLine2D3.StartPoint = catPoint2D3;
            catLine2D3.EndPoint = catPoint2D4;

            Line2D catLine2D4 = catFactory2D1.CreateLine(-5.773503, -10.000000, 5.773503, -10.000000);
            catLine2D4.StartPoint = catPoint2D4;
            catLine2D4.EndPoint = catPoint2D5;

            Line2D catLine2D5 = catFactory2D1.CreateLine(5.773503, -10.000000, 11.547005, -0.000000);
            catLine2D5.StartPoint = catPoint2D5;
            catLine2D5.EndPoint = catPoint2D6;

            Line2D catLine2D6 = catFactory2D1.CreateLine(11.547005, -0.000000, 5.773503, 10.000000);
            catLine2D6.StartPoint = catPoint2D6;
            catLine2D6.EndPoint = catPoint2D1;
            /*
            Line2D catLine2D7 = catFactory2D1.CreateLine(0.000000, 0.000000, 0.000000, 10.000000);
            catLine2D7.StartPoint = catPoint2D7;
            catLine2D7.EndPoint = catPoint2D1;*/
            //catLine2D7.Construction = true;
            
            //2 Kreise einfügen

            Circle2D circle2D1 = catFactory2D1.CreateCircle(0.000000, 0.000000, 4.000000, 0.000000, .000000);  //8 mm durchmesser (Raduis = 4.00)
            //Circle2D circle2D2 = catFactory2D1.CreateCircle(0.000000, 0.000000, 10.000000, 0.000000, .000000);  //20 mm durchmesser (Raduis = 10.00)
            
            //Circles References
            /*
            Reference reference1 = myPart.CreateReferenceFromObject(circle2D2);
            Reference reference8 = myPart.CreateReferenceFromObject(circle2D3);

            //Linien Ref
            Reference reference9 = myPart.CreateReferenceFromObject(catLine2D1); //
            Reference reference10 = myPart.CreateReferenceFromObject(catLine2D2);
            Reference reference11 = myPart.CreateReferenceFromObject(catLine2D3);
            Reference reference12 = myPart.CreateReferenceFromObject(catLine2D4);
            Reference reference13 = myPart.CreateReferenceFromObject(catLine2D5);
            Reference reference14 = myPart.CreateReferenceFromObject(catLine2D6);
            //Points References
            Reference reference15 = myPart.CreateReferenceFromObject(catPoint2D7); //
            Reference reference2 = myPart.CreateReferenceFromObject(catPoint2D1);
            Reference reference3 = myPart.CreateReferenceFromObject(catPoint2D2);
            Reference reference4 = myPart.CreateReferenceFromObject(catPoint2D3);
            Reference reference5 = myPart.CreateReferenceFromObject(catPoint2D4);
            Reference reference6 = myPart.CreateReferenceFromObject(catPoint2D5);
            Reference reference7 = myPart.CreateReferenceFromObject(catPoint2D6);
            
            //constarints
            Constraint constraint1 = myPart.Constraints.AddBiEltCst(CatConstraintType.catCstTypeOn, reference1, reference2);
            Constraint constraint2 = myPart.Constraints.AddBiEltCst(CatConstraintType.catCstTypeOn, reference1, reference3);
            Constraint constraint3 = myPart.Constraints.AddBiEltCst(CatConstraintType.catCstTypeOn, reference1, reference4);
            Constraint constraint4 = myPart.Constraints.AddBiEltCst(CatConstraintType.catCstTypeOn, reference1, reference5);
            Constraint constraint5 = myPart.Constraints.AddBiEltCst(CatConstraintType.catCstTypeOn, reference1, reference6);
            Constraint constraint6 = myPart.Constraints.AddBiEltCst(CatConstraintType.catCstTypeOn, reference1, reference7);

            Constraint constraint7 = myPart.Constraints.AddBiEltCst(CatConstraintType.catCstTypeTangency, reference8, reference9);
            Constraint constraint8 = myPart.Constraints.AddBiEltCst(CatConstraintType.catCstTypeTangency, reference8, reference10);
            Constraint constraint9 = myPart.Constraints.AddBiEltCst(CatConstraintType.catCstTypeTangency, reference8, reference11);
            Constraint constraint10 = myPart.Constraints.AddBiEltCst(CatConstraintType.catCstTypeTangency, reference8, reference12);
            Constraint constraint11 = myPart.Constraints.AddBiEltCst(CatConstraintType.catCstTypeTangency, reference8, reference13);
            Constraint constraint12 = myPart.Constraints.AddBiEltCst(CatConstraintType.catCstTypeTangency, reference8, reference14);
            */
            //Constraint constraint13 = myPart.Constraints.AddBiEltCst(CatConstraintType.catCstTypeMidPoint, reference9, reference15);
            //Modes
            //constraint13.Mode = constraint1.Mode = constraint2.Mode = constraint3.Mode = constraint4.Mode = constraint5.Mode = constraint6.Mode = constraint12.Mode = constraint11.Mode = constraint10.Mode = constraint9.Mode = constraint8.Mode = constraint7.Mode = CatConstraintMode.catCstModeDrivingDimension;
            // Skizzierer verlassen
            hsp_catiaSkizze.CloseEdition();
            // Part aktualisieren
            myPart.Update();
        }

        public void ErzeugeBalken(Double l)
        {
            // Hauptkoerper in Bearbeitung definieren
            myPart.InWorkObject = myPart.MainBody;

            // Block(Balken) erzeugen
            Pad catPad1 = SF.AddNewPad(hsp_catiaSkizze, l);

            // Block umbenennen
            catPad1.set_Name("Balken");

            // Part aktualisieren
            myPart.Update();
        }



    }
}
