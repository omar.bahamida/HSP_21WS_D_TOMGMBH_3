﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
namespace Sprint2
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private int _material = 0;
        private int _GewindeWhal = 0;
        private double _GewLaenge = 0.000;


        private void Sechskant_Selected(object sender, RoutedEventArgs e)
        {

            Sechskant_Grid.Visibility = Visibility.Visible;

        }



        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Combobox_rechteck__Gewinde(object sender, SelectionChangedEventArgs e)
        {
            int selIndex = Combobox_rechteck_Gewinde.SelectedIndex;
            switch (selIndex)
            {
                case 0:
                    _GewindeWhal = 1; //M6
                    break;
                case 1:
                    _GewindeWhal = 2; //M7
                    break;
                case 2:
                    _GewindeWhal = 3; //M8
                    break;
                case 3:
                    _GewindeWhal = 4; //M10
                    break;
                case 4:
                    _GewindeWhal = 5; //M12
                    break;
                default:
                    _GewindeWhal = -1;
                    break;


            }


        }

        private void Combobox_rechteck__Art_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // int selIndex = Combobox_rechteck_Art.SelectedIndex;



        }
        private void Combobox_rechteck__Kopf_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // int selIndex = Combobox_rechteck_Kopf.SelectedIndex;


        }
        private void Combobox_rechteck_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int selIndex = Combobox_rechteck.SelectedIndex;
            switch (selIndex)
            {
                case 0:
                    _material = 1; //Stahl
                    break;
                case 1:
                    _material = 2; //EdelStahl
                    break;
                case 2:
                    _material = 3; //Alum
                    break;
                case 3:
                    _material = 4; //Kupfer
                    break;
                default:
                    _material = -1;
                    break;


            }

        }
        public void Sechskantmutter_Berechnen_Click(object sender, RoutedEventArgs e)
        {
            txtVLM.Text = (Berechnung_Sechskant_Volumen(_material, _GewindeWhal) + "mm2");
            txtMasse.Text = (Berechnung_Sechskant_Masse(_material, _GewindeWhal) + "Kg");
            txtS.Text = (Berechnung_Sechskant_S(_material, _GewindeWhal) + "mm");

            txtSW.Text = (Berechnung_Sechskant_SW(_material, _GewindeWhal) + "");

            txtKosten.Text = (Berechnung_Sechskant_MatKosten(_material, _GewindeWhal) + "Eur");
        }
        public double Berechnung_Sechskant_Volumen(int material, int GewindeType)
        {

            double S = Berechnung_Sechskant_S(_material, _GewindeWhal);
            double g;
            switch (GewindeType)
            {
                case 1:
                    g = 6; //M6
                    break;
                case 2:
                    g = 7; ; //M7
                    break;
                case 3:
                    g = 8; ; //M8
                    break;
                case 4:
                    g = 10; ;//M10
                    break;
                case 5:
                    g = 12; ;//M12
                    break;
                default:
                    g = -1;
                    break;
            }
            double V, r1, r2;
            if (S == 1.5) { r1 = 0.075; r2 = 0.15; V = 2 * S * ((r1 - r2) * (r1 - r2)) * g; return V; }
            else if ((S >= 2) & (S <= 5)) { r1 = 0.125; r2 = 0.25; V = 2 * S * ((r1 - r2) * (r1 - r2)) * g; return V; }
            else if ((S >= 6) & (S <= 12)) { r1 = 0.25; r2 = 0.5; V = 2 * S * ((r1 - r2) * (r1 - r2)) * g; return V; }
            else if ((S >= 13) & (S <= 44)) { r1 = 0.5; r2 = 1; V = 2 * S * ((r1 - r2) * (r1 - r2)) * g; return V; }
            else return 0;

            return V;
        }
        public double Berechnung_Sechskant_Masse(int material, int GewindeType)
        {
            double Masse_SechskantMutter = 0.00;
            switch (GewindeType)
            {
                case 1:
                    Masse_SechskantMutter = 0.0022; //M6
                    break;
                case 2:
                    Masse_SechskantMutter = 0.0030; ; //M7
                    break;
                case 3:
                    Masse_SechskantMutter = 0.0048; ; //M8
                    break;
                case 4:
                    Masse_SechskantMutter = 0.0105; //M10
                    break;
                case 5:
                    Masse_SechskantMutter = 0.0150; //M12
                    break;
                default:
                    Masse_SechskantMutter = -1;
                    break;
            }

            return Masse_SechskantMutter;
        }
        public double Berechnung_Sechskant_S(int material, int GewindeType)
        {
            double S_SechskantMutter = 0.00;
            switch (GewindeType)
            {
                case 1:
                    _GewLaenge = 5; //M6
                    break;
                case 2:
                    _GewLaenge = 5.50; ; //M7
                    break;
                case 3:
                    _GewLaenge = 6.50; ; //M8
                    break;
                case 4:
                    _GewLaenge = 8.00; //M10
                    break;
                case 5:
                    _GewLaenge = 10.00; //M12
                    break;
                default:
                    _GewLaenge = -1;
                    break;
            }

            return S_SechskantMutter = _GewLaenge / 0.613;
        }
        public int Berechnung_Sechskant_SW(int material, int GewindeType)
        {
            int SW_SechskantMutter = 0;
            switch (GewindeType)
            {
                case 1:
                    SW_SechskantMutter = 10; //M6
                    break;
                case 2:
                    SW_SechskantMutter = 11; ; //M7
                    break;
                case 3:
                    SW_SechskantMutter = 13; ; //M8
                    break;
                case 4:
                    SW_SechskantMutter = 17; //M12
                    break;
                case 5:
                    SW_SechskantMutter = 19; //M10
                    break;
                default:
                    SW_SechskantMutter = -1;
                    break;


            }

            return SW_SechskantMutter;
        }
        public double Berechnung_Sechskant_MatKosten(int material, int GewindeType)
        {
            double MatKosten_SechskantMutter = 0.00;
            switch (material)
            {
                case 1:
                    MatKosten_SechskantMutter = 0.32; //Stahl
                    break;
                case 2:
                    MatKosten_SechskantMutter = 0.12; ; //EdelStahl
                    break;
                case 3:
                    MatKosten_SechskantMutter = 2.03; ; //Alum
                    break;
                case 4:
                    MatKosten_SechskantMutter = 0.17; //Kupfer
                    break;
                default:
                    MatKosten_SechskantMutter = -1;
                    break;


            }

            return MatKosten_SechskantMutter;
        }
    }
}

