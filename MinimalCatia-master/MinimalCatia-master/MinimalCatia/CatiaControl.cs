﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinimalCatia
{
    class CatiaControl
    {
        CatiaControl()
        {
            try
            {

                CatiaConnection cc = new CatiaConnection();
                string m;
                Double _durchmesser;
                // Finde Catia Prozess
                if (cc.CATIALaeuft())
                {
                    
                    Console.WriteLine("Bitte Geben Sie die Mutter.");

                    
                    m = Convert.ToString(Console.ReadLine());

                    Console.WriteLine("0");

                    // Öffne ein neues Part
                    cc.ErzeugePart();
                    Console.WriteLine("1");

                    // Erstelle eine Skizze
                    cc.ErstelleLeereSkizze();
                    Console.WriteLine("2");
                    
                    // Generiere ein Profil
                    cc.ErzeugeProfil(20, 10, m);
                    Console.WriteLine("3");

                    // Extrudiere Balken
                    cc.ErzeugeBalken(20);
                    Console.WriteLine("4");
                }
                else
                {
                    Console.WriteLine("Laufende Catia Application nicht gefunden");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception aufgetreten");
            }
            Console.WriteLine("Fertig - Taste drücken.");
            Console.ReadKey();

        }

        static void Main(string[] args)
        {
            new CatiaControl();
        }
    }
}
